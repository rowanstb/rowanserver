#ifndef LOG_H
#define LOG_H

#include <string>

#ifdef ANDROID
#include <android/log.h>
#else
#include <QDebug>
#endif

class Log
{
    public:
        Log() {}

        template<typename... Args>
        static
        void d(const std::string &tag, const std::string &format, Args... args)
        {
#ifdef ANDROID
            __android_log_print(ANDROID_LOG_ERROR, tag.c_str(), format.c_str(), args...);
#else
            std::printf(format.c_str(), args...);
#endif
        }

        static
        void d(const std::string &tag, const std::string &msg)
        {
#ifdef ANDROID
            __android_log_print(ANDROID_LOG_DEBUG, tag.c_str(), "%s", msg.c_str());
#else
            qDebug() << tag.c_str() << msg.c_str();
#endif
        }

        template<typename... Args>
        static
        void e(const std::string &tag, const std::string &format, Args... args)
        {
#ifdef ANDROID
            __android_log_print(ANDROID_LOG_ERROR, tag.c_str(), format.c_str(), args...);
#else
            std::printf(format.c_str(), args...);
#endif
        }

        static
        void e(const std::string &tag, const std::string &msg)
        {
#ifdef ANDROID
            __android_log_print(ANDROID_LOG_ERROR, tag.c_str(), "%s", msg.c_str());
#else
            qCritical() << tag.c_str() << msg.c_str();
#endif
        }
};

#endif // LOG_H