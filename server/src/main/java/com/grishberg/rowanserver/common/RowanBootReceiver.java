package com.grishberg.rowanserver.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.grishberg.rowanserver.data.RcService;

/**
 * Created by grishberg on 21.05.16.
 */
public class RowanBootReceiver extends BroadcastReceiver {
    private static final String TAG = RowanBootReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent = new Intent(context, RcService.class);
        context.startService(serviceIntent);
    }
}
