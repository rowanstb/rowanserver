package com.grishberg.rowanserver;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.grishberg.rowancommon.data.db.DbService;
import com.grishberg.rowancommon.data.db.DbServiceRealmImpl;
import com.grishberg.rowanserver.data.dispatchers.MediaDispatcher;
import com.grishberg.rowanserver.data.mediaPlayer.PlayerControllerImpl;
import com.grishberg.rowanserver.injection.AppComponent;
import com.grishberg.rowanserver.injection.DaggerAppComponent;
import com.grishberg.rowanserver.injection.DbModule;
import com.grishberg.rowanserver.injection.MediaDispatcherModule;
import com.grishberg.rowanserver.injection.PlayerControllerModule;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by grishberg on 16.05.16.
 */
public class App extends Application {
    private static final String TAG = App.class.getSimpleName();
    private static Context sContext;
    private static AppComponent sAppComponent;

    @Override
    public void onCreate() {
        Log.d(TAG, "RowanServer onCreate: v." + BuildConfig.VERSION_NAME);
        super.onCreate();
        sContext = getApplicationContext();
        initUil();
        initRealm();
        DbService dbService = new DbServiceRealmImpl();
        sAppComponent = DaggerAppComponent.builder()
                .dbModule(new DbModule(dbService))
                .playerControllerModule(new PlayerControllerModule(
                        new PlayerControllerImpl(getAppContext(), dbService)))
                .mediaDispatcherModule(new MediaDispatcherModule(new MediaDispatcher()))
                .build();
    }

    private void initRealm() {
        Log.d(TAG, "initRealm() starts");
        RealmConfiguration.Builder builder = new RealmConfiguration.Builder(getApplicationContext())
                .schemaVersion(1);

        builder.deleteRealmIfMigrationNeeded();
        RealmConfiguration defaultConfiguration = builder.build();
        Realm.setDefaultConfiguration(defaultConfiguration);

        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
        } catch (Exception exception) {
            Log.e(TAG, "Can't migrate realm, deleting realm base to not crush", exception);
            Realm.deleteRealm(defaultConfiguration);
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    /**
     * Инициализация Universal Image Loader
     */
    private void initUil() {
        // Create global configuration and initialize ImageLoader with this config
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration
                .Builder(getApplicationContext());
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }

    public static Context getAppContext() {
        return sContext;
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

}
