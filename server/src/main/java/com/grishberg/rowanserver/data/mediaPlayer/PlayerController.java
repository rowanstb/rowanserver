package com.grishberg.rowanserver.data.mediaPlayer;

/**
 * Created by grishberg on 19.06.16.
 */
public interface PlayerController {
    void setMediaPlayerStateListener(MediaPlayerStateListener listener);
    void play();
    void next();
    void prev();
    void seek(int pos);
    void stop();
    void pause();
    void release();
}
