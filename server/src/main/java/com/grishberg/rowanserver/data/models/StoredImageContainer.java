package com.grishberg.rowanserver.data.models;

import java.io.File;
import java.util.Calendar;

/**
 * Created by grishberg on 18.05.16.
 */
public class StoredImageContainer {
    private static final String TAG = StoredImageContainer.class.getSimpleName();
    private long timastamp;
    private String originalFileName;
    private String path;

    public StoredImageContainer(String originalFileName, File file) {
        this.originalFileName = originalFileName;
        this.path = file.getAbsolutePath();
        this.timastamp = Calendar.getInstance().getTimeInMillis();
    }

    public long getTimastamp() {
        return timastamp;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public String getPath() {
        return path;
    }
}
