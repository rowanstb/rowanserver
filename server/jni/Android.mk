LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := Input
LOCAL_SRC_FILES := Input.cpp Injecter.cpp Task.cpp
LOCAL_LDLIBS    := -llog -landroid
LOCAL_CFLAGS    := -Wall -DANDROID 

include $(BUILD_SHARED_LIBRARY)
