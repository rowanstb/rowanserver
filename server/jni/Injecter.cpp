#include "Injecter.h"

#include "Log.h"
#include "Task.h"
#include "uinput.h"

#include <string>
#include <stdio.h>
#include <cstdlib>
#include <memory.h>
#include <iostream>

#include <linux/input.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <cmath>

static const int PRESS = 1;
static const int RELEASE = 0;
static const std::string TAG = "Injecter";
static const int SIZE_EVENT = sizeof(input_event);

Injecter::Injecter()
{
    // ubuntu for test
    // system("sudo chmod +0666 /dev/uinput");
    mFUI = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
    if(mFUI < 0) {
        Log::e(TAG, "/dev/uinput cant not open");
    }
    // init new device
    if (ioctl(mFUI, UI_SET_EVBIT, EV_KEY) < 0) {
        Log::e(TAG, "error EV_KEY");
    }
    if (ioctl(mFUI, UI_SET_EVBIT, EV_SYN) < 0) {
        Log::e(TAG, "error EV_SYN");
    }
    if (ioctl(mFUI, UI_SET_EVBIT, EV_MSC) < 0) {
        Log::e(TAG, "error EV_MSC");
    }

    // mouse
    ioctl(mFUI, UI_SET_KEYBIT, BTN_LEFT);
    ioctl(mFUI, UI_SET_KEYBIT, BTN_RIGHT);
    //FIXME: use EV_REL
    ioctl(mFUI, UI_SET_EVBIT, EV_ABS);
    ioctl(mFUI, UI_SET_ABSBIT, ABS_X);
    ioctl(mFUI, UI_SET_ABSBIT, ABS_Y);

    // keyboard
    for (int i = 0; i < 256; ++i) {
        ioctl(mFUI, UI_SET_KEYBIT, i);
    }
    ioctl(mFUI, UI_SET_KEYBIT, KEY_INFO);
    ioctl(mFUI, UI_SET_KEYBIT, KEY_RED);
    ioctl(mFUI, UI_SET_KEYBIT, KEY_GREEN);
    ioctl(mFUI, UI_SET_KEYBIT, KEY_YELLOW);
    ioctl(mFUI, UI_SET_KEYBIT, KEY_BLUE);
    ioctl(mFUI, UI_SET_KEYBIT, KEY_SCREEN);
    ioctl(mFUI, UI_SET_KEYBIT, KEY_NEXT);
    ioctl(mFUI, UI_SET_KEYBIT, KEY_PREVIOUS);
    ioctl(mFUI, UI_SET_KEYBIT, KEY_AUDIO);
    ioctl(mFUI, UI_SET_KEYBIT, KEY_SUBTITLE);
    ioctl(mFUI, UI_SET_KEYBIT, KEY_CHANNELUP);
    ioctl(mFUI, UI_SET_KEYBIT, KEY_CHANNELDOWN);

    uinput_user_dev uidev;
    memset(&uidev, 0, sizeof(uidev));
    strncpy(uidev.name, "Virtual device for RC", UINPUT_MAX_NAME_SIZE);
    uidev.id.bustype = BUS_USB;
    uidev.id.product = 0xdead;
    uidev.id.vendor  = 0xbeef;
    uidev.id.version = 1;
    write(mFUI, &uidev, sizeof(uidev));
    Log::d(TAG, "create virtual device...");
    if (ioctl(mFUI, UI_DEV_CREATE) < 0) {
        Log::e(TAG, "error UI_DEV_CREATE");
       //FIXME: process error
    } else {
        // wait creating device
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
        Log::d(TAG, "create virtual device ok");
    }
}

Injecter::~Injecter()
{
    ioctl(mFUI, UI_DEV_DESTROY);
    close(mFUI);
    Log::d(TAG, "destroy Injecter");
}

void Injecter::test()
{
    Log::d(TAG, "test inject");
    double val = 0.0;
    while (val < 10.0) {
	mouseMove(500 * (std::sin(val) + 1) / 2.0, 300 * (std::cos(val) + 1) / 2.0);
        usleep(1000 * 15);
        val += 0.1;
    }

    int keyCode = KEY_VOLUMEUP;
    keyPress(keyCode);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    keyCode = KEY_VOLUMEDOWN;
    keyPress(keyCode);
}

void Injecter::keyPress(int keyCode)
{
    injectKey(keyCode, PRESS);
    //TODO: start timer for release
    //injectKey(keyCode, RELEASE);
}

void Injecter::keyRelease(int keyCode)
{
    injectKey(keyCode, RELEASE);
}

void Injecter::injectKey(unsigned long int key, int type)
{
    input_event event;

    memset(&event, 0, SIZE_EVENT);
    gettimeofday(&event.time, NULL);
    event.type = EV_KEY;
    event.code = key;
    event.value = type;
    if (write(mFUI, &event, SIZE_EVENT) < 0) {
        Log::e(TAG, "write key event EV_KEY");
    }

    // sync
    memset(&event, 0, SIZE_EVENT);
    gettimeofday(&event.time, NULL);
    event.type = EV_SYN;
    event.code = SYN_REPORT;
    event.value = 0;
    if(write(mFUI, &event, SIZE_EVENT) < 0) {
        Log::e(TAG, "write key event EV_SYN");
    }
}

void Injecter::mouseMove(int x, int y)
{
    //FIXME: use EV_REL
    input_event event;

    memset(&event, 0, SIZE_EVENT);
    event.type = EV_ABS;
    event.code = ABS_X;
    event.value = x;
    if(write(mFUI, &event, SIZE_EVENT) < 0)
        Log::e(TAG, "write mouse move ABS_X");

    memset(&event, 0, SIZE_EVENT);
    event.type = EV_ABS;
    event.code = ABS_Y;
    event.value = y;
    if(write(mFUI, &event, SIZE_EVENT) < 0)
        Log::e(TAG, "write mouse move ABS_Y");

    memset(&event, 0, SIZE_EVENT);
    event.type = EV_SYN;
    event.code = SYN_REPORT;
    event.value = 0;
    if(write(mFUI, &event, SIZE_EVENT) < 0)
        Log::e(TAG, "write mouse move EV_SYN");
}
