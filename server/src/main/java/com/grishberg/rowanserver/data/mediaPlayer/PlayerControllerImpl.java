package com.grishberg.rowanserver.data.mediaPlayer;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.IntDef;
import android.util.Log;

import com.grishberg.datafacade.data.DataReceiveObserver;
import com.grishberg.datafacade.data.SingleResult;
import com.grishberg.rowancommon.data.db.DbService;
import com.grishberg.rowancommon.mediaPlayer.TrackItem;
import com.grishberg.rowancommon.mediaPlayer.TrackList;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Inject;

/**
 * Created by grishberg on 19.06.16.
 */
public class PlayerControllerImpl implements PlayerController,
        DataReceiveObserver,
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnSeekCompleteListener {
    private static final String TAG = PlayerControllerImpl.class.getSimpleName();

    public static final int PLAYER_STATE_NONE = 0;
    public static final int PLAYER_STATE_PLAYING = 1;
    public static final int PLAYER_STATE_BUFFERING = 2;
    public static final int PLAYER_STATE_BUFFERING_WHILE_PAUSED = 3;
    public static final int PLAYER_STATE_PAUSED = 4;
    public static final int ERROR_TRACK_LIST_EMPTY = 5;

    //причины остановки
    public static final int REASON_END_FILE = 1;
    public static final int REASON_ERROR = 2;
    public static final int REASON_USER_STOP = 3;
    private final Context appContext;

    /**
     * Состояние плеера
     */
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({PLAYER_STATE_NONE,
            PLAYER_STATE_PLAYING,
            PLAYER_STATE_BUFFERING,
            PLAYER_STATE_PAUSED})
    public @interface PlayerState {
    }

    @PlayerState
    private int playerState;

    // текущая позиция
    private int currentPos;
    private final DbService dbService;
    private MediaPlayerStateListener mediaPlayerStateListener;
    private MediaPlayer mediaPlayer;
    private TrackManager trackManager;
    private SingleResult<TrackList> trackListResult;
    private Handler handler;

    @Inject
    public PlayerControllerImpl(Context appContext, DbService dbService) {
        trackManager = new TrackManager();
        trackManager.setRepeateMode(true);
        this.dbService = dbService;
        this.appContext = appContext;
        handler = new Handler(Looper.getMainLooper());
        trackListResult = dbService.getCurrentPlayList();
        trackListResult.addDataReceiveObserver(this);
        playerState = PLAYER_STATE_NONE;
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnErrorListener(this);
        mediaPlayer.setOnSeekCompleteListener(this);
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnCompletionListener(this);
    }

    @Override
    public void onDataReceived() {
        Log.d(TAG, "onDataReceived or changed: ");
        TrackList trackList = trackListResult.getItem();
        if (trackList == null) {
            Log.e(TAG, "onDataReceived: track list is null");
            dbService.addNewTrackList("");
            trackListResult = dbService.getCurrentPlayList();
        }
        trackManager.setCurrentTrackList(trackList);
    }

    @Override
    public void setMediaPlayerStateListener(MediaPlayerStateListener mediaPlayerStateListener) {
        this.mediaPlayerStateListener = mediaPlayerStateListener;
    }

    public TrackList getCuttentTrackList() {
        return trackManager.getCurrentTrackList();
    }

    @Override
    public void play() {
        handler.post(this::doPlay);
    }

    @Override
    public void next() {
        Log.d(TAG, "next: ");
        handler.post(() -> {
            if (trackManager.getNextTrack() != null) {
                Log.d(TAG, "next: has next item");
                doPlay();
            }
        });
    }

    @Override
    public void prev() {
        Log.d(TAG, "prev: ");
        handler.post(() -> {
            TrackItem trackItem = trackManager.getPrevTrack();
            if (trackItem == null) {
                Log.e(TAG, "play: track list is empty");
                if (mediaPlayerStateListener != null) {
                    mediaPlayerStateListener.onError(ERROR_TRACK_LIST_EMPTY);
                }
                return;
            }
            try {
                if (playerState != PLAYER_STATE_NONE) {
                    mediaPlayer.reset();
                    playerState = PLAYER_STATE_NONE;
                }
                Log.d(TAG, "play: path = " + trackItem.getFileName());
                mediaPlayer.setDataSource(trackItem.getFileName());
                mediaPlayer.prepareAsync();
                playerState = PLAYER_STATE_PLAYING;
            } catch (IOException e) {
                playerState = PLAYER_STATE_NONE;
            }
        });
    }

    @Override
    public void seek(int pos) {
        Log.d(TAG, "seek: ");
        handler.post(() -> {
            if (playerState == PLAYER_STATE_PLAYING) {
                playerState = PLAYER_STATE_BUFFERING;
                mediaPlayer.seekTo(pos * 1000);
            }
        });
    }

    @Override
    public void stop() {
        Log.d(TAG, "stop: ");
        if (playerState != PLAYER_STATE_NONE) {
            playerState = PLAYER_STATE_NONE;
            mediaPlayer.stop();
            mediaPlayer.reset();
        }
    }

    @Override
    public void pause() {
        if (playerState == PLAYER_STATE_PLAYING) {
            playerState = PLAYER_STATE_PAUSED;
            Log.d(TAG, "pause: pausing");
            mediaPlayer.pause();
            return;
        }
        if (playerState == PLAYER_STATE_PAUSED) {
            playerState = PLAYER_STATE_PLAYING;
            Log.d(TAG, "pause: resuming");
            mediaPlayer.start();
        }
    }

    /**
     * Воспроизведение текущей позиции в плейлисте
     */
    private void doPlay() {
        TrackItem trackItem = trackManager.getCurrentTrack();
        if (trackItem == null) {
            Log.e(TAG, "play: track list is empty");
            if (mediaPlayerStateListener != null) {
                mediaPlayerStateListener.onError(ERROR_TRACK_LIST_EMPTY);
            }
            return;
        }
        try {
            Log.d(TAG, "play: path = " + trackItem.getFileName());
            mediaPlayer.reset();
            mediaPlayer.setDataSource(trackItem.getFileName());
            mediaPlayer.prepareAsync();
            playerState = PLAYER_STATE_BUFFERING;
        } catch (IOException e) {
            playerState = PLAYER_STATE_NONE;
        } catch (IllegalStateException e) {
            Log.e(TAG, "play: ", e);
        }
    }

    //---------- MediaPlayer state listener implementation ---------

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.d(TAG, "onError() called with: " + "mp = [" + mp + "], what = [" + what + "], extra = [" + extra + "]");
        mediaPlayer.stop();
        mediaPlayer.reset();
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.d(TAG, "onCompletion: " + playerState);
        if (playerState == PLAYER_STATE_PLAYING) {
            mediaPlayer.stop();
            mediaPlayer.reset();
            next();
        }
        playerState = PLAYER_STATE_NONE;
        if (mediaPlayerStateListener != null) {
            mediaPlayerStateListener.onStop(REASON_END_FILE);
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.d(TAG, "onPrepared: start playing");
        mediaPlayer.start();
        playerState = PLAYER_STATE_PLAYING;
        if (mediaPlayerStateListener != null) {
            mediaPlayerStateListener.onStared();
        }
    }

    @Override
    public void onSeekComplete(MediaPlayer mp) {
        Log.d(TAG, "onSeekComplete: ");
        if (mediaPlayerStateListener != null) {
            mediaPlayerStateListener.onResumed(currentPos);
        }
        playerState = PLAYER_STATE_PLAYING;
    }

    @Override
    public void release() {
        Log.d(TAG, "release: ");
        if (trackListResult != null) {
            trackListResult.removeDataReceiveObserver(this);
            trackListResult.release();
        }

        if (mediaPlayer != null) {
            mediaPlayer.release();
        }
    }
}
