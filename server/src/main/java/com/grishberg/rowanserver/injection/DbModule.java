package com.grishberg.rowanserver.injection;

import com.grishberg.rowancommon.data.db.DbService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by grishberg on 19.06.16.
 */
@Module
public class DbModule {
    private static final String TAG = DbModule.class.getSimpleName();

    private final DbService dbService;

    public DbModule(DbService dbService) {
        this.dbService = dbService;
    }

    @Provides
    @Singleton
    DbService provideDbService() {
        return dbService;
    }
}
