/*
 * Later.h
 *
 *  Created on: 19 авг. 2014 г.
 *      Author: vadim
 */

#include <functional>
#include <chrono>
#include <future>
#include <cstdio>
#include <thread>

#include "Log.h"

#ifndef TASK_H_
#define TASK_H_

class Task {
    public:
        Task() {}

        void singleShot(int delay, std::function<void(void)> callback) {
            mIsCancel = false;
            std::thread tr([=] {
                std::this_thread::sleep_for(std::chrono::milliseconds(delay));
                if (!mIsCancel)
                    callback();
            });
            tr.detach();
        }

        void cancel() {
            mIsCancel = true;
        }

    private:
        bool mIsCancel = false;
};

#endif /* TASK_H_ */
