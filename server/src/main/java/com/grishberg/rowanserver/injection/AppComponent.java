package com.grishberg.rowanserver.injection;

import com.grishberg.rowanserver.data.RcService;
import com.grishberg.rowanserver.data.mediaPlayer.PlayerControllerImpl;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by grishberg on 19.06.16.
 */
@Singleton
@Component(modules = {
        DbModule.class,
        AppModule.class,
        MediaDispatcherModule.class,
        PlayerControllerModule.class})
public interface AppComponent {
    void inject(PlayerControllerImpl playerController);

    void inject(RcService rcService);
}
