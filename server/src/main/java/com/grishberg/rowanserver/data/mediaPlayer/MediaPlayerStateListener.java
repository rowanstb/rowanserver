package com.grishberg.rowanserver.data.mediaPlayer;

/**
 * Created by grishberg on 19.06.16.
 */
public interface MediaPlayerStateListener{
    void onStared();
    void onPaused(int pos);
    void onResumed(int pos);
    void onBuffering();
    void onStop(int reason);
    void onError(int errorCode);
}
