package com.grishberg.rowanserver.data.dispatchers;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;

/**
 * Created by grishberg on 18.05.16.
 */
public class FileDispatcher {
    private static final String TAG = FileDispatcher.class.getSimpleName();
    public static final String ANDROID_DATA = "/Android/data/";
    public static final String FILES = "files";

    public static String getExt(String fileName) {
        if (!TextUtils.isEmpty(fileName)) {
            int pos = fileName.lastIndexOf('.');
            return fileName.substring(pos + 1);
        }
        return "tmp";
    }

    /**
     * Create a File for saving an image
     */
    public static File getOutputMediaFile(Context context,String mediaType, String fileName) {
        File mediaStorageDir = null;
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        if (Environment.MEDIA_MOUNTED.equals(Environment
                .getExternalStorageState())) {

            mediaStorageDir = new File(
                    Environment.getExternalStorageDirectory()
                            + ANDROID_DATA
                            + context.getPackageName()
                            + File.separator
                            + FILES
                            + File.separator + mediaType);
        } else {
            mediaStorageDir = new File("/tmp/" + FILES+ File.separator + mediaType);
        }

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                if (context.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE")
                        == PackageManager.PERMISSION_DENIED) {
                    Log.e(TAG, ">> We don't have permission to write - please add it.");
                } else {
                    Log.e(TAG, "We do have permission - the problem lies elsewhere.");
                }
                return null;
            }
        }
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + fileName);
        return mediaFile;
    }

    public static void deleteFile(String path) {
        File file = new File(path);
        if (file.exists()) {
            file.delete();
        }
    }
}
