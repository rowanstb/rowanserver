package com.grishberg.rowanserver.data.dispatchers;

import android.content.Context;
import android.util.Log;

import com.grishberg.rowancommon.data.FileUtils;
import com.grishberg.rowanserver.data.models.StoredImageContainer;

import java.io.File;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by grishberg on 18.05.16.
 */
public class MediaDispatcher {
    private static final String TAG = MediaDispatcher.class.getSimpleName();
    private static final int MAX_CACHE_SIZE = 5;
    public static final String AUDIO = "audio";

    private final Queue<StoredImageContainer> cache;

    public MediaDispatcher() {
        cache = new ConcurrentLinkedQueue<>();
    }

    /**
     * @param data
     * @return
     */
    public String storeImage(Context context, String name, byte[] data) {
        String generatedName = UUID.randomUUID().toString() + '.' + FileDispatcher.getExt(name);
        File file = FileDispatcher.getOutputMediaFile(context, "images", generatedName);
        if (file == null) {
            Log.e(TAG, "storeImage: file permission");
            return null;
        }
        FileUtils.saveToFile(file, data);
        // добавить элемент в кэш
        cache.add(new StoredImageContainer(name, file));
        // удалить лишние элементы из кэша
        if (cache.size() > MAX_CACHE_SIZE) {
            StoredImageContainer oldObject = cache.poll();
            if (oldObject != null) {
                FileDispatcher.deleteFile(oldObject.getPath());
            }
        }
        return file.getAbsolutePath();
    }

    /**
     * Сохранить аудио файл
     *
     * @param context
     * @param fileName
     * @param data
     * @return
     */
    public String storeAudio(Context context, String fileName, byte[] data) {
        File file = FileDispatcher.getOutputMediaFile(context,
                AUDIO, fileName);
        if (file == null) {
            Log.e(TAG, "storeImage: file permission");
            return null;
        }
        FileUtils.saveToFile(file, data);
        return file.getAbsolutePath();
    }

    public String getPath(String name) {
        for (StoredImageContainer container : cache) {
            if (container.getOriginalFileName().equals(name)) {
                return container.getPath();
            }
        }
        return null;
    }
}
