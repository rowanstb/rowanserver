package com.grishberg.rowanserver.ui.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.grishberg.rowancommon.interfaces.BoundListener;
import com.grishberg.rowancommon.interfaces.FragmentsRegistrable;
import com.grishberg.rowancommon.interfaces.HandlerReceiver;
import com.grishberg.rowanserver.R;
import com.grishberg.rowanserver.common.ui.fragments.BaseFragment;
import com.grishberg.rowanserver.data.RcService;
import com.grishberg.rowanserver.data.models.ImageInfoContainer;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;

public class GalleryFragment extends BaseFragment implements BoundListener {
    private static final String TAG = GalleryFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ImageView ivImage;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    private FragmentsRegistrable<RcService> mListener;

    public GalleryFragment() {
        // Required empty public constructor
    }

    public static GalleryFragment newInstance() {
        GalleryFragment fragment = new GalleryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(false)
                .cacheOnDisk(false)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .build();
        setRetainInstance(true);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gallery, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ivImage = (ImageView) view.findViewById(R.id.ivImage);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentsRegistrable) {
            mListener = (FragmentsRegistrable) context;
            mListener.register(this);
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener.unregister(this);
        mListener = null;
    }

    public void displayImage(String originalFileName, String path) {
        if (ivImage != null) {
            String decodedUri = Uri.fromFile(new File(path)).toString();
            imageLoader.displayImage(decodedUri, ivImage, options);
            showContentTitle(originalFileName);
        }
    }

    @Override
    public void onServiceBound(HandlerReceiver service) {

    }

    @Override
    public void onServiceUnBound(HandlerReceiver service) {

    }

    @Override
    public void onMessageReceived(int what, Object obj) {
        Log.d(TAG, "onMessageReceived: ");
        if (what == RcService.ACTION_SHOW_IMAGE) {
            if (obj instanceof ImageInfoContainer) {
                displayImage(((ImageInfoContainer) obj).getOriginalName(),
                        ((ImageInfoContainer) obj).getPath());
            }
        }
    }
}
