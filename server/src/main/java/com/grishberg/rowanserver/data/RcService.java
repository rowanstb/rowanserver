package com.grishberg.rowanserver.data;

import android.app.KeyguardManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Parcelable;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.grishberg.rowancommon.KeysConst;
import com.grishberg.rowancommon.NetworkingConst;
import com.grishberg.rowancommon.data.ParcelableConverter;
import com.grishberg.rowancommon.data.db.DbService;
import com.grishberg.rowancommon.data.models.MessagesContainer;
import com.grishberg.rowancommon.data.models.commands.InfoCmd;
import com.grishberg.rowancommon.data.models.commands.KeyPressCmd;
import com.grishberg.rowancommon.data.models.commands.PlayerCmd;
import com.grishberg.rowancommon.data.models.commands.SendFileCmd;
import com.grishberg.rowancommon.data.services.HandlerReceiverImpl;
import com.grishberg.rowancommon.mediaPlayer.TrackItem;
import com.grishberg.rowanserver.App;
import com.grishberg.rowanserver.R;
import com.grishberg.rowanserver.common.Constants;
import com.grishberg.rowanserver.data.dispatchers.MediaDispatcher;
import com.grishberg.rowanserver.data.mediaPlayer.PlayerController;
import com.grishberg.rowanserver.data.models.ImageInfoContainer;
import com.grishberg.rowanserver.ui.activities.MainActivity;
import com.grishberg.rowanserver.data.dispatchers.Input;
import com.grishberg.utils.network.ConnectionReceiver;
import com.grishberg.utils.network.ConnectionReceiverImpl;
import com.grishberg.utils.network.interfaces.OnAcceptedListener;
import com.grishberg.utils.network.interfaces.OnCloseConnectionListener;
import com.grishberg.utils.network.interfaces.OnConnectionErrorListener;
import com.grishberg.utils.network.interfaces.OnMessageListener;
import com.grishberg.utils.network.interfaces.OnServerConnectionEstablishedListener;
import com.grishberg.utils.network.tcp.server.TcpServer;
import com.grishberg.utils.network.tcp.server.TcpServerImpl;

import java.io.IOException;
import java.util.Iterator;
import java.util.Locale;

import javax.inject.Inject;

public class RcService extends HandlerReceiverImpl implements OnMessageListener,
        OnConnectionErrorListener, OnAcceptedListener, OnServerConnectionEstablishedListener, OnCloseConnectionListener {
    private static final String TAG = RcService.class.getSimpleName();
    public static final int ACTION_ACCEPTED = 1;
    public static final int ACTION_CONNECTED_TO_SERVER = 2;
    public static final int ACTION_MESSAGE = 3;
    public static final int ACTION_SHOW_IMAGE = 4;
    public static final int ACTION_CONNECTION_ERROR = -1;

    private TcpServer tcpServer;
    private ConnectionReceiver connectionReceiver;
    private Input keyEmulatorDispatcher;

    /**
     * плеер
     */
    @Inject
    DbService dbService;

    @Inject
    PlayerController playerController;

    @Inject
    MediaDispatcher imageDispatcher;

    public RcService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //start as foreground service
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_rowan);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("Rowan server")
                .setTicker("Rowan server controller")
                .setContentText("My Music")
                .setSmallIcon(R.drawable.ic_rowan)
                .setLargeIcon(
                        Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .build();
        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE,
                notification);

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate: ");
        super.onCreate();
        App.getAppComponent().inject(this);

        try {
            keyEmulatorDispatcher = new Input();
            tcpServer = new TcpServerImpl(NetworkingConst.TCP_PORT, this, this, this);
            connectionReceiver = new ConnectionReceiverImpl(NetworkingConst.BROADCAST_PORT,
                    NetworkingConst.BACK_TCP_PORT);
            connectionReceiver.setConnectionListener(this);
            connectionReceiver.start();
            startListening();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (connectionReceiver != null) {
            connectionReceiver.stop();
        }
        if (playerController != null) {
            playerController.setMediaPlayerStateListener(null);
            playerController.release();
        }
        stopListening();
    }

    public void startListening() {
        Log.d(TAG, "startListeningServers: ");
        tcpServer.start();
    }

    public void stopListening() {
        Log.d(TAG, "stopListeningServers: ");
        tcpServer.stop();
    }

    /**
     * Регистрация хэндлера для взаимодействия с активностью
     *
     * @param handler
     */
    public void registerHandler(Handler handler) {
        this.activityHandler = handler;
        Iterator<MessagesContainer> iterator = messagesQueue.iterator();
        while (iterator.hasNext()) {
            sendMessageToActivity(iterator.next());
            iterator.remove();
        }
    }

    /**
     * снятие регистрации хэндлера
     *
     * @param handler
     */
    public void unregisterHandler(Handler handler) {
        this.activityHandler = null;
    }

    @Override
    public void onAccepted(String address) {
        sendMessageToActivity(new MessagesContainer(ACTION_ACCEPTED, address));
    }

    @Override
    public void onConnectionEstablished(String address) {
        Log.d(TAG, String.format(Locale.US, "onConnectionEstablished: %s", address));
    }

    public void sendMessage(String address, Parcelable cmd) {
        byte[] packet = ParcelableConverter.convertToArray(cmd);
        tcpServer.sendMessage(address, packet);
    }

    @Override
    public void onReceivedMessage(String address, byte[] bytes) {
        Parcelable msg = ParcelableConverter.convertFromArray(bytes);
        // Нажатие кнопки
        if (msg instanceof KeyPressCmd) {
            KeyPressCmd keyPressCmd = (KeyPressCmd) msg;
            if (keyPressCmd.getKey() == KeysConst.KEY_HOME &&
                    keyPressCmd.isDown()) {
                keyEmulatorDispatcher.home();
                return;
            }
            if (keyPressCmd.isDown()) {
                keyEmulatorDispatcher.onKeyPressed(keyPressCmd.getKey());
            } else {
                keyEmulatorDispatcher.onKeyReleased(keyPressCmd.getKey());
            }
            return;
        } else if (msg instanceof SendFileCmd) { // Отправка файла
            processReceiveFile((SendFileCmd) msg);
            return;
        } else if (msg instanceof InfoCmd) { // отправка команды
            processInfoCmd(address, (InfoCmd) msg);
            return;
        } else if (msg instanceof PlayerCmd) {
            processPlayerCmd((PlayerCmd) msg);
            return;
        }
        sendMessageToActivity(new MessagesContainer(ACTION_MESSAGE, msg));
    }

    /**
     * Обработка команды для плеера
     *
     * @param msg
     */
    private void processPlayerCmd(@NonNull PlayerCmd msg) {
        Log.d(TAG, "processPlayerCmd: " + msg.getCmd());
        switch (msg.getCmd()) {
            case PlayerCmd.CMD_PLAY:
                Log.d(TAG, "processPlayerCmd: CMD_PLAY");
                playerController.play();
                break;
            case PlayerCmd.CMD_NEXT:
                Log.d(TAG, "processPlayerCmd: CMD_NEXT");
                playerController.next();
                break;
            case PlayerCmd.CMD_PREV:
                Log.d(TAG, "processPlayerCmd: CMD_PREV");
                playerController.prev();
                break;
            case PlayerCmd.CMD_PAUSE:
                Log.d(TAG, "processPlayerCmd: CMD_PAUSE");
                playerController.pause();
                break;
            case PlayerCmd.CMD_STOP:
                Log.d(TAG, "processPlayerCmd: CMD_STOP");
                playerController.stop();
                break;
            case PlayerCmd.CMD_SEEK:
                Log.d(TAG, "processPlayerCmd: CMD_SEEK");
                playerController.seek(msg.getParameter1());
                break;
        }
    }

    private void processReceiveFile(SendFileCmd msg) {
        if (activityHandler == null) {
            MainActivity.startActivity(this);
        }
        String imagePath;
        switch (msg.getFileType()) {
            case SendFileCmd.BUFFERED_IMAGE:
                // сохранить изображение в кэш
                imagePath = imageDispatcher.storeImage(getApplicationContext(),
                        msg.getFileName(),
                        msg.getData());
                break;
            case SendFileCmd.CURRENT_IMAGE:
                imagePath = imageDispatcher.storeImage(getApplicationContext(),
                        msg.getFileName(),
                        msg.getData());
                sendMessageToActivity(new MessagesContainer(ACTION_SHOW_IMAGE,
                        new ImageInfoContainer(msg.getFileName(), imagePath)));
                break;
            case SendFileCmd.AUDIO:
                Log.d(TAG, "processReceiveFile: AUDIO");
                imagePath = imageDispatcher.storeAudio(getApplicationContext(),
                        msg.getFileName(),
                        msg.getData());
                TrackItem trackItem = new TrackItem();
                trackItem.setFileName(imagePath);
                trackItem.setName(msg.getFileName());
                trackItem.setEnabled(true);
                dbService.addTrackItem("", trackItem);
                playerController.play();
                break;
        }

        unlockScreen();
    }

    private void processInfoCmd(String address, InfoCmd msg) {
        switch (msg.getType()) {
            case InfoCmd.CHECK_FILE_EXISTS:
                if (imageDispatcher.getPath(msg.getDataStr()) != null) {
                    tcpServer.sendMessage(address,
                            ParcelableConverter.convertToArray(
                                    new InfoCmd(msg.getId(),
                                            InfoCmd.CHECK_FILE_EXISTS,
                                            1)));
                } else {
                    tcpServer.sendMessage(address,
                            ParcelableConverter.convertToArray(
                                    new InfoCmd(msg.getId(),
                                            InfoCmd.CHECK_FILE_EXISTS,
                                            0)));
                }
                break;
        }
    }

    @Override
    public void onError(Throwable throwable) {
        sendMessageToActivity(new MessagesContainer(ACTION_CONNECTION_ERROR, throwable));
    }

    /**
     * разблокировать экран от скринсейвера
     */
    public void unlockScreen() {
        Log.d(TAG, "unlockScreen: ");
        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
        wakeLock.acquire();

        KeyguardManager keyguardManager = (KeyguardManager) getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("TAG");
        keyguardLock.disableKeyguard();
        wakeLock.release();
    }

    @Override
    public void onCloseConnection(String s) {
        Log.d(TAG, "onCloseConnection: ");
    }
}
