package com.grishberg.rowanserver.injection;

import android.content.Context;

import com.grishberg.rowanserver.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by grishberg on 19.06.16.
 */
@Module
public class AppModule {
    private final App application;

    public AppModule(App app) {
        this.application = app;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return application.getApplicationContext();
    }
}
