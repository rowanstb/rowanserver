package com.grishberg.rowanserver.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.grishberg.rowancommon.ui.activities.BaseActivity;
import com.grishberg.rowanserver.R;
import com.grishberg.rowanserver.data.RcService;
import com.grishberg.rowanserver.data.models.ImageInfoContainer;
import com.grishberg.rowanserver.ui.fragments.GalleryFragment;

public class MainActivity extends BaseActivity<RcService> {

    public static void startActivity(Context context){
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = new Intent(this, RcService.class);
        startService(intent);
    }

    /**
     * Получено сообщение от сервиса
     *
     * @param what
     * @param obj
     */
    @Override
    protected void onMessageReceived(int what, Object obj) {
        // Отобразить картинку
        switch (what){
            case RcService.ACTION_SHOW_IMAGE:
                if(!(obj instanceof ImageInfoContainer)) return;

                GalleryFragment fragment = (GalleryFragment) getSupportFragmentManager()
                        .findFragmentByTag(GalleryFragment.class.getSimpleName());
                if (fragment == null) {
                    fragment = GalleryFragment.newInstance();
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.mainContent, fragment, GalleryFragment.class.getSimpleName())
                            .commit();
                    getSupportFragmentManager().executePendingTransactions();
                }
                fragment.displayImage(((ImageInfoContainer) obj).getOriginalName(),
                        ((ImageInfoContainer) obj).getPath());
                break;
        }
    }

    @Override
    protected Intent getServiceIntent() {
        return new Intent(this, RcService.class);
    }
}
