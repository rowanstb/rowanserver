#include "Task.h"

#ifndef INJECTKEY_H
#define INJECTKEY_H

class Injecter
{
    public:
        explicit Injecter();
        ~Injecter();

        void test();
        void keyPress(int keyCode);
        void keyRelease(int keyCode);
        void mouseMove(int x, int y);

    private:
        void injectKey(unsigned long injectKey, int type);

        Task mRelease;
        int mFUI;
};

#endif // INJECTKEY_H
