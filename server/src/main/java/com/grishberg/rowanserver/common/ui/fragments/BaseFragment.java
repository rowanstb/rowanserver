package com.grishberg.rowanserver.common.ui.fragments;

import android.os.Handler;
import android.support.v4.app.Fragment;

/**
 * Created by grishberg on 18.05.16.
 */
public class BaseFragment extends Fragment {
    private static final String TAG = BaseFragment.class.getSimpleName();
    private Handler hideTitleHandler;

    protected void showContentTitle(String title) {
        //showTitleBar();
        //hideTitleHandler.postDelayed(hideTitleRunnable, DELAY_MILLIS);
    }

    private void hideTitleBar() {

    }

    private Runnable hideTitleRunnable = new Runnable() {
        @Override
        public void run() {
            hideTitleBar();
        }
    };
}
