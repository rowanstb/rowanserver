package com.grishberg.rowanserver.common;

/**
 * Created by grishberg on 17.05.16.
 */
public class Constants {
    private static final String TAG = Constants.class.getSimpleName();
    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
    }
    public final class Actions{
        public static final String STOP_FOREGROUND_ACTION = "stopForegroundAction";
        public static final String START_FOREGROUND_ACTION = "startForegroundAction";
    }
}
