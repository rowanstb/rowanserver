package com.grishberg.rowanserver.data.dispatchers;

import android.content.Intent;
import android.util.Log;

import com.grishberg.rowancommon.KeysConst;
import com.grishberg.rowanserver.App;

/**
 * Created by grishberg on 16.05.16.
 */
public class Input {
    private static final String TAG = Input.class.getSimpleName();
    public static final int HOME_FLAG = 274726912;

    static {
        try {
            System.loadLibrary("Input");
        } catch (UnsatisfiedLinkError e) {
            Log.e(TAG, "Library Input failed to load.", e);
        }
    }

    private static native void pressKey(int key);

    private static native void releaseKey(int key);

    private static native void mouseMove(int x, int y);

    private static native void test();

    public Input() {
    }

    public void onKeyPressed(int keyCode) {
        try {
            pressKey(keyCode);
        } catch (UnsatisfiedLinkError e) {
            Log.e(TAG, "onKeyPressed: ", e);
        }
    }

    public void onKeyReleased(int keyCode) {
        try {
            releaseKey(keyCode);
        } catch (UnsatisfiedLinkError e) {
            Log.e(TAG, "onKeyPressed: ", e);
        }
    }

    public void left(Boolean isPressed) {
        Log.d(TAG, "call Input.left");
        if (isPressed) {
            onKeyPressed(KeysConst.KEY_LEFT);
        } else {
            onKeyReleased(KeysConst.KEY_LEFT);
        }
    }

    public void right(Boolean isPressed) {
        Log.d(TAG, "call Input.right");
        if (isPressed) {
            onKeyPressed(KeysConst.KEY_RIGHT);
        } else {
            onKeyReleased(KeysConst.KEY_RIGHT);
        }
    }

    public void up(Boolean isPressed) {
        Log.d(TAG, "call Input.up");
        if (isPressed) {
            onKeyPressed(KeysConst.KEY_UP);
        } else {
            onKeyReleased(KeysConst.KEY_UP);
        }
    }

    public void down(Boolean isPressed) {
        Log.d(TAG, "call Input.down");
        if (isPressed) {
            onKeyPressed(KeysConst.KEY_DOWN);
        } else {
            onKeyReleased(KeysConst.KEY_DOWN);
        }
    }

    public void select() {
        Log.d(TAG, "call Input.select");
        pressKey(KeysConst.KEY_ENTER);
        releaseKey(KeysConst.KEY_ENTER);
    }

    public void back() {
        Log.d(TAG, "call Input.back");
        pressKey(KeysConst.KEY_ESC);
        releaseKey(KeysConst.KEY_ESC);
    }

    public void volumeUp(Boolean isPressed) {
        Log.d(TAG, "call Input.volumeUp");
        if (isPressed) {
            onKeyPressed(KeysConst.KEY_VOLUMEUP);
        } else {
            onKeyReleased(KeysConst.KEY_VOLUMEUP);
        }
    }

    public void volumeDown(Boolean isPressed) {
        Log.d(TAG, "call Input.volumeDown");
        if (isPressed) {
            onKeyPressed(KeysConst.KEY_VOLUMEDOWN);
        } else {
            onKeyReleased(KeysConst.KEY_VOLUMEDOWN);
        }
    }

    public void sendText() {
        Log.d(TAG, "call Input.sendText");
    }

    public void home() {
        Log.d(TAG, "call Input.home");
        Intent i = new Intent(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_HOME);
        i.addFlags(HOME_FLAG);
        App.getAppContext().startActivity(i);

        //pressKey(Keys.KEY_HOME);
        //releaseKey(Keys.KEY_HOME);
    }
}
