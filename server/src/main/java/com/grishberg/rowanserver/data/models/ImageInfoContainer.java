package com.grishberg.rowanserver.data.models;

/**
 * Created by grishberg on 20.05.16.
 */
public class ImageInfoContainer {
    private static final String TAG = ImageInfoContainer.class.getSimpleName();
    private String originalName;
    private String path;

    public ImageInfoContainer(String originalName, String path) {
        this.originalName = originalName;
        this.path = path;
    }

    public String getOriginalName() {
        return originalName;
    }

    public String getPath() {
        return path;
    }
}
