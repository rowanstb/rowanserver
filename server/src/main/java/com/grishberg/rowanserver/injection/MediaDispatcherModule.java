package com.grishberg.rowanserver.injection;

import com.grishberg.rowanserver.data.dispatchers.MediaDispatcher;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by grishberg on 19.06.16.
 */
@Module
public class MediaDispatcherModule {
    private static final String TAG = MediaDispatcherModule.class.getSimpleName();
    private final MediaDispatcher mediaDispatcher;

    public MediaDispatcherModule(MediaDispatcher mediaDispatcher) {
        this.mediaDispatcher = mediaDispatcher;
    }

    @Provides
    @Singleton
    MediaDispatcher provideDbService() {
        return mediaDispatcher;
    }
}
