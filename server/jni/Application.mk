APP_ABI               := armeabi armeabi-v7a
APP_PLATFORM          := android-16
NDK_TOOLCHAIN_VERSION := 4.9
APP_STL               := gnustl_static
APP_CPPFLAGS          := -std=c++11
# APP_CPPFLAGS        += -pthread -frtti -fexceptions