package com.grishberg.rowanserver.data.mediaPlayer;

import android.util.Log;

import com.grishberg.rowancommon.mediaPlayer.TrackItem;
import com.grishberg.rowancommon.mediaPlayer.TrackList;

/**
 * Created by grishberg on 19.06.16.
 * Управление списком воспроизведения
 */
public class TrackManager {
    private static final String TAG = TrackManager.class.getSimpleName();
    private TrackList currentTrackList;
    private int currentTrackIndex;
    private boolean isRepeateMode;

    public void setCurrentTrackList(TrackList currentTrackList) {
        this.currentTrackList = currentTrackList;
    }

    public void setCurrentTrackIndex(int currentTrackIndex) {
        this.currentTrackIndex = currentTrackIndex;
    }

    public TrackList getCurrentTrackList() {
        return currentTrackList;
    }

    public boolean isRepeateMode() {
        return isRepeateMode;
    }

    public void setRepeateMode(boolean repeateMode) {
        isRepeateMode = repeateMode;
    }

    /**
     * Следующий трэк с учетом выбранного режима воспроизведения
     *
     * @return
     */
    public TrackItem getCurrentTrack() {
        if (currentTrackList == null) {
            return null;
        }
        if (currentTrackIndex == currentTrackList.getTracks().size()) {
            currentTrackIndex = currentTrackList.getTracks().size() - 1;
        }
        return currentTrackList.getTracks().get(currentTrackIndex);
    }

    /**
     * Следующий трэк с учетом выбранного режима воспроизведения
     *
     * @return
     */
    public TrackItem getNextTrack() {
        if (currentTrackList == null) {
            return null;
        }
        currentTrackIndex++;
        Log.d(TAG, String.format("getNextTrack: tracks size = %d, index = %d",
                currentTrackList.getTracks().size(), currentTrackIndex));
        if (currentTrackIndex == currentTrackList.getTracks().size()) {
            if (!isRepeateMode) {
                currentTrackIndex = currentTrackList.getTracks().size() - 1;
                return null;
            }
            currentTrackIndex = 0;
        }
        return currentTrackList.getTracks().get(currentTrackIndex);
    }

    public TrackItem getPrevTrack() {
        if (currentTrackList == null) {
            return null;
        }
        currentTrackIndex--;
        if (currentTrackIndex < 0) {
            if (!isRepeateMode) {
                currentTrackIndex = 0;
                return null;
            }
            currentTrackIndex = currentTrackList.getTracks().size() - 1;
        }
        return currentTrackList.getTracks().get(currentTrackIndex);
    }
}
