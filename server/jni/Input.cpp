#include <jni.h>
#include <android/log.h>

#include <linux/input.h>

#include <chrono>
#include <thread>

#include "com_grishberg_rowanserver_data_dispatchers_Input.h"
#include "Log.h"
#include "Injecter.h"

const static std::string TAG = "Input";

Injecter *injecter = nullptr;

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
  JNIEnv* env;
  if (vm->GetEnv((void**) &env, JNI_VERSION_1_6) != JNI_OK)
    return -1;

  Log::d(TAG, "JNI_OnLoad");

  if (injecter == nullptr) {
    injecter = new Injecter();
  }

  return JNI_VERSION_1_6;
}

JNIEXPORT void JNICALL Java_com_grishberg_rowanserver_data_dispatchers_Input_test(JNIEnv * env, jclass)
{
  Log::d(TAG, "run test");
  injecter->test();
}

JNIEXPORT void JNICALL Java_com_grishberg_rowanserver_data_dispatchers_Input_pressKey(JNIEnv * env, jclass, jint key)
{
  injecter->keyPress(key);
}

JNIEXPORT void JNICALL Java_com_grishberg_rowanserver_data_dispatchers_Input_releaseKey(JNIEnv * env, jclass, jint key)
{
  injecter->keyRelease(key);
}

JNIEXPORT void JNICALL Java_com_grishberg_rowanserver_data_dispatchers_Input_mouseMove(JNIEnv * env, jclass, jint x, jint y)
{
  injecter->mouseMove(x, y);
}