package com.grishberg.rowanserver.injection;

import android.content.Context;

import com.grishberg.rowanserver.data.mediaPlayer.PlayerController;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by grishberg on 19.06.16.
 */
@Module
public class PlayerControllerModule {
    private static final String TAG = PlayerControllerModule.class.getSimpleName();

    private final PlayerController playerController;

    public PlayerControllerModule(PlayerController playerController) {
        this.playerController = playerController;
    }

    @Provides
    @Singleton
    PlayerController provideContext() {
        return playerController;
    }
}
